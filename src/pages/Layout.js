import React from 'react';
import { Container } from 'reactstrap';
import NavbarContainer from '../containers/NavbarContainer';

export default class Layout extends React.Component {
  render() {
    return (
      <div>
        <NavbarContainer />
        <Container>
          {this.props.children}
        </Container>
      </div>
    );
  }
}
