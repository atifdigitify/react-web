import React from 'react';
import Layout from './Layout';

export default class AboutPage extends React.Component {
  render() {
    return (
      <div>
        <Layout>
          {this.props.children}
        </Layout>
      </div>
    );
  }
}
