const UserReducer = (state = [], action) => {
  switch (action.type) {
    case 'REGISTER_USER':
      return dispatch => dispatch({
        ...state,
        action
      });
    default:
      return {};
  }
};
export default UserReducer;
