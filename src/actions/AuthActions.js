const RegisterUserAction = form => ({
  type: 'REGISTER_USER',
  fullName: form.fullName,
  email: form.email,
  password: form.password
});
export default RegisterUserAction;

