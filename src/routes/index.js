import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AboutPage from '../pages/AboutPage';
import ContactPage from '../pages/ContactPage';
import DashboardPage from '../pages/DashboardPage';
import HomePage from '../pages/HomePage';
import LoginPage from '../pages/LoginPage';
import RegisterPage from '../pages/RegisterPage';
import ProfilePage from '../pages/ProfilePage';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" exact component={HomePage} />
      <Route path="/dashboard" component={DashboardPage} />
      <Route path="/profile/" component={ProfilePage} />
      <Route path="/about/" component={AboutPage} />
      <Route path="/contact/" component={ContactPage} />
      <Route path="/register/" component={RegisterPage} />
      <Route path="/login/" component={LoginPage} />
    </Switch>
  </BrowserRouter>
);

export default Routes;

