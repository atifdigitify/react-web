import React from 'react';
import { connect } from 'react-redux';
import { RegisterUserAction } from '../actions/AuthActions';
import SignupComponent from '../components/SignupComponent';

class SignupContainer extends React.Component {
  constructor(props) {
    super();
  }

  render() {
    return (
      <div>
        <SignupComponent onSubmit={this.props.onSubmit} />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onSubmit: (data) => {
    console.log('Calling dipatch');
    dispatch(RegisterUserAction(data));
  }
});

export default connect(null, mapDispatchToProps)(SignupContainer);
