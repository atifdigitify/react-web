import React from 'react';
import ProfileComponent from '../components/ProfileComponent';

class ProfileContainer extends React.Component {
  render() {
    return (
      <div>
        <ProfileComponent />
      </div>
    );
  }
}

export default ProfileContainer;
