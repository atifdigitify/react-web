import React from 'react';
import NavbarComponent from '../components/NavbarComponent';

class NavbarContainer extends React.Component {
  render() {
    return (
      <div>
        <NavbarComponent />
      </div>
    );
  }
}

export default NavbarComponent;
