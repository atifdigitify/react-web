import React, { Component } from 'react';
import { Button, Col, Form, FormGroup, Input, Label, Row } from 'reactstrap';
import PropTypes from 'prop-types';
import Layout from '../pages/Layout';

class SignupComponent extends Component {

  static propTypes = {
    fullName: PropTypes.string,
    email: PropTypes.string,
    password: PropTypes.string,
    onSubmit: PropTypes.func
  };

  constructor(props) {
    super();
    this.submitBtnClick = this.submitBtnClick.bind(this);
  }

  submitBtnClick(event) {
    event.preventDefault();
    const form = event.target;
    const data = new FormData(form);

    const formDate = {};

    for (let name of data.keys()) {
      const input = form.elements[ name ];
      formDate[ input.name ] = data.get(name);
    }

    this.props.onSubmit(formDate);
  }

  render() {
    return (
      <Layout>
        <Form onSubmit={this.submitBtnClick}>
          <Row>
            <Col
              lg={{
                size: 6,
                offset: 3
              }}
              sm="12"
            >
              <FormGroup row>
                <h2>Register</h2>
              </FormGroup>
              <FormGroup row>
                <Input id="fullName" name="fullName" placeholder="Enter full name" />
              </FormGroup>
              <FormGroup row>
                <Input id="email" name="email" placeholder="Enter email" />
              </FormGroup>
              <FormGroup row>
                <Input id="password" name="password" placeholder="Enter password" />
              </FormGroup>
              <FormGroup row>
                <Input id="confirmPassword" name="confirmPassword"
                       placeholder="Enter confirm password" />
              </FormGroup>
              <FormGroup row>
                <Input id="checkbox" name="checkbox" type="checkbox" />
                <Label for="chkb">Do you agree with terms?</Label>
              </FormGroup>
              <FormGroup row>
                <Button>Submit</Button>
              </FormGroup>
            </Col>
          </Row>
        </Form>
      </Layout>
    );
  }
}

export default SignupComponent;
