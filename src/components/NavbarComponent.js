import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
  UncontrolledDropdown
} from 'reactstrap';

class NavbarComponent extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      isOpen: false,
      collapsed: true
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render() {
    return (
      <Navbar color="primary" light expand="md">
        <NavbarBrand href="/">YAP</NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <Link href="#" to="/about">
                <NavLink>About</NavLink>
              </Link>
            </NavItem>
            <NavItem>
              <Link href="#" to="/contact">
                <NavLink>Contact</NavLink>
              </Link>
            </NavItem>
            <NavItem>
              <Link href="#" to="/register">
                <NavLink>Register</NavLink>
              </Link>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Me
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem>
                  <Link href="#" to="/profile">
                    <NavLink>Profile</NavLink>
                  </Link>
                </DropdownItem>
                <DropdownItem>
                  <Link href="#" to="/dashboard">
                    <NavLink>Dashboard</NavLink>
                  </Link>
                </DropdownItem>
                <DropdownItem>
                  <Link href="#" to="/settings">
                    <NavLink>Settings</NavLink>
                  </Link>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Collapse>
      </Navbar>
    );
  }
}

export default NavbarComponent;
